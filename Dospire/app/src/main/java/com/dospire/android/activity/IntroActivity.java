package com.dospire.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dospire.android.R;
import com.dospire.android.base.CommonActivity;
import com.dospire.android.commons.Commons;
import com.dospire.android.commons.Constants;

public class IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Commons.g_isAppRunning = true;
        gotoMain();
    }

    public void gotoMain(){

        new Handler().postDelayed(new Runnable(){

            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
