package com.dospire.android.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by YueXi on 3/21/2017.
 */

public class FontTypeface {

    private Context context;

    public FontTypeface(Context context){
        this.context = context;
    }

    public Typeface getTypefaceAndroid(){
        Typeface typeFace = Typeface.DEFAULT;
        String strFont = "assets/font/lucida_sans.ttf";
        try {
            if (!strFont.equals("")){
                String strLeft = strFont.substring(0, 12);
                if (strLeft.equals("assets/font/")){
                    typeFace = Typeface.createFromAsset(context.getAssets(), strFont.replace("assets/", ""));
                } else {
                    typeFace = Typeface.createFromFile(strFont);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return typeFace;
    }
}
