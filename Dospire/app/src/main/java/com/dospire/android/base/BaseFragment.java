package com.dospire.android.base;


import android.support.v4.app.Fragment;

/**
 * Created by YueXi on 11/9/2016.
 */

public class BaseFragment extends Fragment {

    protected BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void closeProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        _context.showToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }

    public void showAlertDialog(String msg){

        _context.showAlertDialog(msg);
    }
}
