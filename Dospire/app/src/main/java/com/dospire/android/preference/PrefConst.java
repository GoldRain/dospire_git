package com.dospire.android.preference;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {

    public static final String PREFKEY_USERNAME = "uname";
    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREF_REGISTERED = "registered";
    public static final String PREF_TOKENID = "token_id";
    public static final String PREFKEY_HIDEAD = "hide_ad";

}
