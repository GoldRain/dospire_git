package com.dospire.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dospire.android.R;
import com.dospire.android.adapter.ProfileListViewAdapter;
import com.dospire.android.base.CommonActivity;
import com.dospire.android.commons.Commons;
import com.dospire.android.model.PostEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountActivity extends CommonActivity{

    CircleImageView imv_photo;
    TextView txv_name, txv_location, txv_follow_num;

    @BindView(R.id.imv_inspire)ImageView ui_imvInspire;
    @BindView(R.id.imv_noty) ImageView ui_imvNoti;
    @BindView(R.id.imv_post) ImageView ui_imvPost;
    @BindView(R.id.imv_explore) ImageView ui_imvExplore;
    @BindView(R.id.imv_profile) ImageView ui_imvProfile;

    @BindView(R.id.imv_settings) ImageView imv_setting;
    @BindView(R.id.imv_hot) ImageView imv_hot;
    @BindView(R.id.imv_pin) ImageView imv_pin;
    @BindView(R.id.imv_share) ImageView imv_share;

    ArrayList<PostEntity> _all_post = new ArrayList<>();

    ListView lst_profile;
    ProfileListViewAdapter adapter_profile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);

        for (int i =0; i <10; i++){

            PostEntity postEntity = new PostEntity();

            postEntity.setId(i);
            postEntity.setLocation("@Yanji "+ ", " + "Jilin "+ ", " + "China");
            postEntity.setLike_count(i);
            postEntity.setTitle("Professional human being.I also\nlove eyebrows!");
            postEntity.setVote_count(i);

            if (i%4 == 0) {

                //postEntity.setPhoto_url("http://35.165.175.25/uploadfiles/pet/2017/07/89_pet14993943635.jpg");

                postEntity.set_photo_url(R.drawable.ic_img2);


            } else if (i%4 == 1) {

                postEntity.set_photo_url(R.drawable.ic_img1);
            }
            else if (i%4 == 2) {

                postEntity.set_photo_url(R.drawable.ic_img3);

            } else if (i%4 == 3) {

                postEntity.set_photo_url(R.drawable.ic_img4);
            }

            _all_post.add(postEntity);

        }

        loadLaout();
    }

    private void loadLaout() {

        imv_photo = (CircleImageView)findViewById(R.id.imv_account_phot);
        txv_name = (TextView)findViewById(R.id.txv_name);
        txv_location = (TextView)findViewById(R.id.txv_location);
        txv_follow_num =(TextView)findViewById(R.id.txv_follow_num);

        lst_profile = (ListView)findViewById(R.id.lst_profile);
        adapter_profile = new ProfileListViewAdapter(this,_all_post );
        lst_profile.setAdapter(adapter_profile);


        setFoot(R.drawable.ic_inspire_g,R.drawable.ic_explore_g, R.drawable.ic_post_g, R.drawable.ic_noty_g, R.drawable.ic_profile_b);

        showProfile();
    }

    private void showProfile(){

        if (Commons.g_user.getPhoto_url().length() > 0)
            Glide.with(this).load(Commons.g_user.getPhoto_url()).placeholder(R.drawable.bg_non_profile).into(imv_photo);

        txv_name.setText("Professional human being.I also\nlove eyebrows!");
        txv_follow_num.setText(String.valueOf(Commons.g_user.getFollower_count()));

    }

    public void setFoot(int a, int b, int c, int d, int e){

        ui_imvInspire.setImageResource(a); ui_imvExplore.setImageResource(b);
        ui_imvPost.setImageResource(c); ui_imvNoti.setImageResource(d);  ui_imvProfile.setImageResource(e);

    }

    @OnClick(R.id.lyt_noty) void gotoNoti(){

        startActivity(new Intent(this, NotificationActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_post) void gotoPost(){
        startActivity(new Intent(this, PostActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_inspire) void gotoInspire(){

        startActivity(new Intent(this, InspireActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_explore) void gotoExplore(){

        startActivity(new Intent(this, ExploreActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_profile) void gotoProfile(){

        startActivity(new Intent(this, AccountActivity.class));
        overridePendingTransition(0,0);

        finish();
    }


    @OnClick(R.id.imv_settings) void showSetting(){

        showToast("Click Setting");
    }
    @OnClick(R.id.imv_hot) void showHOt(){
        showToast("Click..");
    }

}
