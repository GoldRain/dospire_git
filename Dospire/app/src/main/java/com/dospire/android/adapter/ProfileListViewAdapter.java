package com.dospire.android.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dospire.android.R;
import com.dospire.android.activity.AccountActivity;
import com.dospire.android.model.PostEntity;

import java.util.ArrayList;

/**
 * Created by ITWhiz4U on 7/11/2017.
 */
public class ProfileListViewAdapter extends BaseAdapter {

    private static final int TYPE_LEFT = 0;
    private static final int TYPE_RIGHT = 1;

    ArrayList<PostEntity> _allPost = new ArrayList<>();
    AccountActivity _activity;

    public ProfileListViewAdapter(AccountActivity activity, ArrayList<PostEntity> posts){

        this._activity = activity;
        this._allPost = posts;
    }

    @Override
    public int getCount() {
        return _allPost.size();
    }

    @Override
    public Object getItem(int position) {
        return _allPost.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {

        if (position%2  ==  0){

            return TYPE_LEFT;
        }

        return TYPE_RIGHT;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type) {

            case TYPE_LEFT: {

                ProfileHolder_Left holder;

                if (convertView == null){

                    holder = new ProfileHolder_Left();

                    LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_profile_list_left, parent, false);

                    holder.imv_background = (ImageView)convertView.findViewById(R.id.imv_background);
                    holder.txv_headline = (TextView)convertView.findViewById(R.id.txv_headline);
                    holder.txv_like_num = (TextView)convertView.findViewById(R.id.txv_like_num);
                    holder.txv_vote_num = (TextView)convertView.findViewById(R.id.txv_vote_num);

                    convertView.setTag(holder);

                } else {

                    holder = (ProfileHolder_Left)convertView.getTag();
                }

                PostEntity postEntity = _allPost.get(position);

                Glide.with(_activity).load(postEntity.get_photo_url()).placeholder(R.drawable.ic_img1).into(holder.imv_background);
                //holder.txv_headline.setText(postEntity.getTitle());
                holder.txv_headline.setText("Professional human being.!");
                holder.txv_like_num.setText(String.valueOf(postEntity.getLike_count()));
                holder.txv_vote_num.setText(String.valueOf(postEntity.getVote_count()));

            }
            break;

            case TYPE_RIGHT: {

                ProfileHolder_Right holder_r;

                if (convertView == null){

                    holder_r = new ProfileHolder_Right();

                    LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_profile_list_right, parent, false);

                    holder_r.imv_background_r = (ImageView)convertView.findViewById(R.id.imv_background);
                    holder_r.txv_headline_r = (TextView)convertView.findViewById(R.id.txv_headline_r);
                    holder_r.txv_like_num_r = (TextView)convertView.findViewById(R.id.txv_like_num_r);
                    holder_r.txv_vote_num_r = (TextView)convertView.findViewById(R.id.txv_vote_num_r);

                    convertView.setTag(holder_r);

                } else {

                    holder_r = (ProfileHolder_Right)convertView.getTag();
                }

                PostEntity postEntity = _allPost.get(position);

                Glide.with(_activity).load(postEntity.get_photo_url()).placeholder(R.drawable.ic_img1).into(holder_r.imv_background_r);
                //holder_r.txv_headline_r.setText(postEntity.getTitle());
                holder_r.txv_headline_r.setText("I love eyebrows!");
                holder_r.txv_like_num_r.setText(String.valueOf(postEntity.getLike_count()));
                holder_r.txv_like_num_r.setText(String.valueOf(postEntity.getVote_count()));

            }

            break;
        }

        return convertView;
    }

    public class ProfileHolder_Left{

        ImageView imv_background;
        TextView txv_headline, txv_like_num, txv_vote_num;
    }

    public class ProfileHolder_Right{

        ImageView imv_background_r;
        TextView txv_headline_r, txv_like_num_r, txv_vote_num_r;
    }
}
