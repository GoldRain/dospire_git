package com.dospire.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dospire.android.R;
import com.dospire.android.activity.NotificationActivity;
import com.dospire.android.model.UserEntity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ITWhiz4U on 7/9/2017.
 */
public class ChatListAdapter extends BaseAdapter {

    NotificationActivity _activity;
    ArrayList<UserEntity> _allUsers = new ArrayList<>();

    public ChatListAdapter (NotificationActivity activity, ArrayList<UserEntity> users){

        this._activity  = activity;
        this._allUsers = users;
    }

    @Override
    public int getCount() {
        return _allUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return _allUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChatListHolder holder;
        if (convertView  ==  null){

            holder = new ChatListHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_chatlist, parent, false);

            holder.imv_photo = (CircleImageView)convertView.findViewById(R.id.imv_photo);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_location = (TextView)convertView.findViewById(R.id.txv_location);

            convertView.setTag(holder);
        } else {

            holder = (ChatListHolder)convertView.getTag();
        }

        final UserEntity user = _allUsers.get(position);

        Glide.with(_activity).load(user.getPhoto_url()).placeholder(R.drawable.ic_img1).into(holder.imv_photo);
        holder.txv_name.setText(user.getUsername());
        holder.txv_location.setText(user.getLocation());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //_activity.gotoChatting(user);`````
            }
        });

        return convertView;
    }

    public class ChatListHolder{

        CircleImageView imv_photo;
        TextView txv_name, txv_location;
    }
}
