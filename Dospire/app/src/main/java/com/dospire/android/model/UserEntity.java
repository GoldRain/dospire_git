package com.dospire.android.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by YueXi on 7/7/2017.
 */

public class UserEntity implements Serializable {

    int id = 0;
    String username = "";
    String email = "";
    String photo_url = "";
    String bg_url = "";
    String location = "";
    int follower_count = 0;
    ArrayList<PostEntity> posts = new ArrayList<>();

    int _bg_url = 0;
    int _photo_url = 0;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getBg_url() {
        return bg_url;
    }

    public void setBg_url(String bg_url) {
        this.bg_url = bg_url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(int follower_count) {
        this.follower_count = follower_count;
    }

    public ArrayList<PostEntity> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<PostEntity> posts) {
        this.posts = posts;
    }

    public int get_bg_url() {
        return _bg_url;
    }

    public void set_bg_url(int _bg_url) {
        this._bg_url = _bg_url;
    }

    public int get_photo_url() {
        return _photo_url;
    }

    public void set_photo_url(int _photo_url) {
        this._photo_url = _photo_url;
    }
}
