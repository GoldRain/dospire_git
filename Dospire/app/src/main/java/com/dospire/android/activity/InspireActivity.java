package com.dospire.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dospire.android.R;
import com.dospire.android.adapter.InspireListAdapter;
import com.dospire.android.base.CommonActivity;
import com.dospire.android.commons.Commons;
import com.dospire.android.commons.Constants;
import com.dospire.android.model.UserEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InspireActivity extends CommonActivity{

    @BindView(R.id.lst_inspire) ListView lst_inspire;
    InspireListAdapter _adapter;
    ArrayList<UserEntity> _inspires = new ArrayList<>();

    @BindView(R.id.toolbar_actionbar)Toolbar ui_toolbar;
    @BindView(R.id.imv_inspire)ImageView ui_imvInspire;
    @BindView(R.id.imv_noty) ImageView ui_imvNoti;
    @BindView(R.id.imv_post) ImageView ui_imvPost;
    @BindView(R.id.imv_explore) ImageView ui_imvExplore;
    @BindView(R.id.imv_profile) ImageView ui_imvProfile;
    @BindView(R.id.txv_menu)TextView ui_txvMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspire);
        ButterKnife.bind(this);

        for (int i = 0; i < 10; i++){

            UserEntity inspire = new UserEntity();

            inspire.setUsername("Jinthreek89");
            inspire.setId(i);

            inspire.setFollower_count(i);

            if (i%4 == 0) {

                inspire.setLocation("@Yanji "+ ", " + "Jilin "+ ", " + "China");
                inspire.set_bg_url(R.drawable.ic_img2);
                inspire.set_photo_url(R.drawable.ic_img2);

            } else if (i%4 == 1) {

                inspire.setLocation("@Atlantic "+ ", " + "Ocean "+ " " + "American");
                inspire.set_bg_url(R.drawable.ic_img1);
                inspire.set_photo_url(R.drawable.ic_img1);
               /* inspire.setBg_url("http://35.165.175.25/uploadfiles/pet/2017/07/85_pet14989983598.jpg");
                inspire.setPhoto_url("http://35.165.175.25/uploadfiles/pet/2017/07/85_pet14989983598.jpg");*/
            }
            else if (i%4 == 2) {

                inspire.setLocation("@Marseille"+ ", " + "France ");
                inspire.set_bg_url(R.drawable.ic_img3);
                inspire.set_photo_url(R.drawable.ic_img3);
                /*inspire.setBg_url("http://35.165.175.25/uploadfiles/pet/2017/07/86_pet14989991402.jpg");
                inspire.setPhoto_url("http://35.165.175.25/uploadfiles/pet/2017/07/86_pet14989991402.jpg");*/

            } else if (i% 4 == 3){

                inspire.setLocation("@Roma"+ ", " + "Italy ");
                inspire.set_bg_url(R.drawable.ic_img4);
                inspire.set_photo_url(R.drawable.ic_img4);
            }

            Commons.g_user =  inspire;

            _inspires.add(inspire);

        }

        Constants.Common_User = _inspires;

        loadLayout();
    }

    private void loadLayout() {

        setSupportActionBar(ui_toolbar);
        ui_toolbar.inflateMenu(R.menu.toolbar_menu_setting);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setFoot(R.drawable.ic_inspire_b,R.drawable.ic_explore_g, R.drawable.ic_post_g, R.drawable.ic_noty_g, R.drawable.ic_profile_g);

        _adapter = new InspireListAdapter(this, _inspires);
        lst_inspire.setAdapter(_adapter);


    }


    @OnClick(R.id.lyt_noty) void gotoNoti(){

        startActivity(new Intent(this, NotificationActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_post) void gotoPost(){
        startActivity(new Intent(this, PostActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_explore) void gotoExplore(){

        startActivity(new Intent(this, ExploreActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_profile) void gotoProfile(){

        startActivity(new Intent(this, AccountActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    public void gotoChatting(UserEntity user){

        Constants.CHAT_LIST = 5;

        Intent intent = new Intent(this, ChattingActivity.class);
        intent.putExtra(Constants.KEY_CHATTING, user);
        startActivity(intent);

        overridePendingTransition(0,0);

        finish();
    }

    public void setFoot(int a, int b, int c, int d, int e){

        ui_imvInspire.setImageResource(a); ui_imvExplore.setImageResource(b);
        ui_imvPost.setImageResource(c); ui_imvNoti.setImageResource(d);  ui_imvProfile.setImageResource(e);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu_setting, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.tb_setting:
                return true;

            case R.id.popup_following:
                showToast("Clicked Following");
                ui_txvMenu.setText(getString(R.string.following));
                return true;

            case R.id.popup_map:
                showToast("Clicked Map");
                ui_txvMenu.setText(getString(R.string.map));
                return true;

            case R.id.popup_nearby:
                showToast("Clicked Nearby");
                ui_txvMenu.setText(getString(R.string.nearby));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        onExit();
    }
}
