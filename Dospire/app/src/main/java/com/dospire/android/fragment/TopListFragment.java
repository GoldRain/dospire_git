package com.dospire.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.dospire.android.R;
import com.dospire.android.activity.ExploreActivity;
import com.dospire.android.adapter.ExploreListViewAdapter;
import com.dospire.android.commons.Constants;

@SuppressLint("ValidFragment")
public class TopListFragment extends Fragment {

    ExploreActivity _activity;
    View view;
    ListView lst_top_list;
    ExploreListViewAdapter _adapter_top_list;

    public TopListFragment(ExploreActivity activity) {
        // Required empty public constructor

        this._activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_top_list, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        lst_top_list = (ListView)view.findViewById(R.id.lst_top_list);
        _adapter_top_list = new ExploreListViewAdapter(_activity, Constants.Common_User);
        lst_top_list.setAdapter(_adapter_top_list);

    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        _activity = (ExploreActivity)context;
    }
}
