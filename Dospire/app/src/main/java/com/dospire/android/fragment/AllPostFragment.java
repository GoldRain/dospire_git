package com.dospire.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.dospire.android.R;
import com.dospire.android.activity.ExploreActivity;
import com.dospire.android.adapter.ExploreListViewAdapter;
import com.dospire.android.adapter.InspireListAdapter;
import com.dospire.android.commons.Constants;

import java.util.List;

@SuppressLint("ValidFragment")
public class AllPostFragment extends Fragment {

    ExploreActivity _activity;
    View view;
    ListView lst_all_post;
    ExploreListViewAdapter _adapter_all;

    public AllPostFragment(ExploreActivity activity) {
        // Required empty public constructor

        this._activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_post, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        lst_all_post = (ListView)view.findViewById(R.id.lst_all_post);
        _adapter_all = new ExploreListViewAdapter(_activity, Constants.Common_User);
        lst_all_post.setAdapter(_adapter_all);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (ExploreActivity) context;
    }
}
