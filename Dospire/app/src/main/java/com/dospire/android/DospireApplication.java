package com.dospire.android;

import android.app.Application;

/**
 * Created by YueXi on 7/4/2017.
 */

public class DospireApplication extends Application {

    public static final String TAG = DospireApplication.class.getSimpleName();
    private static DospireApplication _instance;


    @Override
    public void onCreate(){

        super.onCreate();
        _instance = this;

    }

    public static synchronized DospireApplication getInstance(){

        return _instance;
    }
}
