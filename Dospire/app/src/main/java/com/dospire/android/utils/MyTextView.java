package com.dospire.android.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Snowflake on 8/5/2016.
 */
public class MyTextView extends AppCompatTextView {

    public MyTextView(Context context, AttributeSet attrs, int defStyle){

        super(context, attrs, defStyle);
        init();
    }

    public MyTextView(Context context, AttributeSet attrs){

        super(context, attrs);
        init();
    }

    public MyTextView(Context context){

        super(context);
        init();
    }

    public void init(){

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/lucida_sans.ttf");
        setTypeface(tf, 1);
    }
}
