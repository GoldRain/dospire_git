package com.dospire.android.base;

import android.os.Bundle;

import java.util.Locale;

/**
 * Created by Snowflake on 7/27/2016.
 */
public abstract class CommonActivity extends BaseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
    }

    public String getLanguage(){

        Locale systemLocale = getResources().getConfiguration().locale;
        String strLanguage = systemLocale.getLanguage();

        return strLanguage;
    }
}
