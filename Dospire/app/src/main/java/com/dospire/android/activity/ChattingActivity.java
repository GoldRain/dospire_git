package com.dospire.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dospire.android.R;
import com.dospire.android.base.CommonActivity;
import com.dospire.android.commons.Constants;
import com.dospire.android.model.UserEntity;

public class ChattingActivity extends CommonActivity implements View.OnClickListener {

    UserEntity user;

    TextView txv_title, txv_send;
    EditText edt_message;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);

        user = (UserEntity)getIntent().getSerializableExtra(Constants.KEY_CHATTING);

        loadLayout();
    }

    private void loadLayout() {

        edt_message = (EditText)findViewById(R.id.edt_message);
        txv_title =(TextView)findViewById(R.id.txv_title);
        txv_title.setText(user.getUsername());

        txv_send = (TextView)findViewById(R.id.txv_send);
        txv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edt_message.setText("");
                showToast("Sending...");
            }
        });

        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Constants.CHAT_LIST == 5)
                    gotoBack();

                else if (Constants.CHAT_LIST == 10)
                    gotoChatList();
            }
        });

    }

    private void gotoBack(){

        startActivity(new Intent(ChattingActivity.this, InspireActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private void gotoChatList(){

        startActivity(new Intent(this, NotificationActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void onBackPressed() {

        if (Constants.CHAT_LIST == 5)
            gotoBack();

        else if (Constants.CHAT_LIST == 10)
            gotoChatList();
    }
}
