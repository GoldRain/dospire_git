package com.dospire.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.dospire.android.R;
import com.dospire.android.adapter.ChatListAdapter;
import com.dospire.android.base.CommonActivity;
import com.dospire.android.commons.Constants;
import com.dospire.android.model.UserEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationActivity extends CommonActivity {

    LinearLayout lyt_chat, lyt_post, lyt_inspire, lyt_explore, lyt_profile;

    @BindView(R.id.imv_inspire) ImageView imv_inspire;
    @BindView(R.id.imv_explore) ImageView imv_explore;
    @BindView(R.id.imv_post) ImageView imv_post;
    @BindView(R.id.imv_noty) ImageView imv_noty;
    @BindView(R.id.imv_profile) ImageView imv_profile;

    ListView lst_chatlist;
    ChatListAdapter _adapter;
    ArrayList<UserEntity> _allUsers = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);

        _allUsers  = Constants.Common_User;

        loadLayout();
    }

    private void loadLayout() {


        setFood(R.drawable.ic_inspire_g,R.drawable.ic_explore_g, R.drawable.ic_post_g, R.drawable.ic_noty_b, R.drawable.ic_profile_g);
        lst_chatlist = (ListView)findViewById(R.id.lst_chatlist);
        _adapter = new ChatListAdapter(this, _allUsers);
        lst_chatlist.setAdapter(_adapter);

    }

    public void setFood(int a, int b, int c, int d, int e){

        imv_inspire.setImageResource(a); imv_explore.setImageResource(b);imv_post.setImageResource(c); imv_noty.setImageResource(d);  imv_profile.setImageResource(e);

    }

    @OnClick(R.id.lyt_noty) void gotoNoti(){

        startActivity(new Intent(this, NotificationActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_post) void gotoPost(){
        startActivity(new Intent(this, PostActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_inspire) void gotoInspire(){

        startActivity(new Intent(this, InspireActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_explore) void gotoExplore(){

        startActivity(new Intent(this, ExploreActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_profile) void gotoProfile(){

        startActivity(new Intent(this, AccountActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    /*public void gotoChatting(UserEntity user){

        Constants.CHAT_LIST = 10;

        Intent intent = new Intent(this, ChattingActivity.class);
        intent.putExtra(Constants.KEY_CHATTING, user);
        startActivity(intent);
    }*/



    @Override
    public void onBackPressed() {
        onExit();
    }
}
