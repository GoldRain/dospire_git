package com.dospire.android.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dospire.android.R;
import com.dospire.android.base.CommonActivity;
import com.dospire.android.commons.Constants;
import com.dospire.android.utils.BitmapUtils;
import com.dospire.android.utils.CustomAutoCompleteTextView;
import com.dospire.android.utils.MediaRealPathUtil;
import com.dospire.android.utils.PlaceJSONParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PostActivity extends CommonActivity implements View.OnClickListener {

    ImageView imv_noty,imv_post, imv_inspire, imv_explore, imv_profile;

    LinearLayout lyt_chat, lyt_post, lyt_inspire, lyt_explore, lyt_profile;
    EditText edt_title ;
    CustomAutoCompleteTextView edt_location;
    PlacesTask placesTask;
    ParserTask parserTask;
    ImageView imv_photo;
    ImageView imv_play;
    TextView txv_addphoto;
    @BindView(R.id.btn_sub_post)TextView ui_txvSubPost;

    ImageView imv_3_sec,imv_4_sec, imv_5_sec, imv_6_sec,imv_7_sec,imv_8_sec,imv_9_sec, imv_10_sec, imv_forever;

    private Uri _imageCaptureUri;
    String _photoPath = "";
    String _thumPath, videoPath = "", _thumbPath = "";

    int[] strIds = {R.string.sub_post};
    int[] _selectIdx = {-1};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        ButterKnife.bind(this);

        checkLocationPermission();
        doTakePhoto();
        loadLayout();
    }

    private void loadLayout() {

        imv_noty = (ImageView)findViewById(R.id.imv_noty);
        imv_post = (ImageView)findViewById(R.id.imv_post);
        imv_inspire = (ImageView)findViewById(R.id.imv_inspire);
        imv_explore = (ImageView)findViewById(R.id.imv_explore);
        imv_profile = (ImageView)findViewById(R.id.imv_profile);

        ui_txvSubPost.setOnClickListener(this);


        lyt_chat = (LinearLayout)findViewById(R.id.lyt_noty);
        lyt_chat.setOnClickListener(this);

        lyt_post = (LinearLayout)findViewById(R.id.lyt_post);
        lyt_post.setOnClickListener(this);

        lyt_inspire = (LinearLayout)findViewById(R.id.lyt_inspire);
        lyt_inspire.setOnClickListener(this);

        lyt_explore =(LinearLayout)findViewById(R.id.lyt_explore);
        lyt_explore.setOnClickListener(this);

        lyt_profile = (LinearLayout)findViewById(R.id.lyt_profile);
        lyt_profile.setOnClickListener(this);

        edt_title = (EditText)findViewById(R.id.edt_title);
        edt_location = (CustomAutoCompleteTextView) findViewById(R.id.edt_location);
        edt_location.setThreshold(1);

        edt_location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                placesTask = new PlacesTask();
                placesTask.execute(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        txv_addphoto = (TextView)findViewById(R.id.txv_addphoto);

        imv_photo = (ImageView)findViewById(R.id.imv_addphoto);
        //imv_photo.setOnClickListener(this);

        imv_play = (ImageView)findViewById(R.id.imv_play);
        imv_play.setVisibility(View.GONE);

        LinearLayout lyt_container = (LinearLayout)findViewById(R.id.lyt_container);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_title.getWindowToken(), 0);

                return false;
            }
        });

        setFood(R.drawable.ic_inspire_g,R.drawable.ic_explore_g, R.drawable.ic_post_b, R.drawable.ic_noty_g, R.drawable.ic_profile_g);


    }

    private void gotoNoti(){

        startActivity(new Intent(this, NotificationActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private void gotoPost(){
        startActivity(new Intent(this, PostActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    private void gotoInspire(){

        startActivity(new Intent(this, InspireActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    private void gotoExplore(){

        startActivity(new Intent(this, ExploreActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    private void gotoProfile(){

        startActivity(new Intent(this, AccountActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    public void setFood(int a, int b, int c, int d, int e){

        imv_inspire.setImageResource(a); imv_explore.setImageResource(b);imv_post.setImageResource(c); imv_noty.setImageResource(d);  imv_profile.setImageResource(e);

    }

    public void selectPhoto() {

        final String[] items = {"Take photo", /*"Choose from Gallery"*/ "Take video","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeVideo();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }

    }

    public void doTakeVideo(){

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,  BitmapUtils.getVideoThumbFolderPath());
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        startActivityForResult(intent,Constants.PICK_FROM_VIDEO);
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.PICK_FROM_VIDEO:

                if (resultCode == RESULT_OK) {

                    Uri w_uri = data.getData();

                    String w_strPath = MediaRealPathUtil.getPath(this, w_uri);

                    if (w_strPath == null) {
                        showToast(getString(R.string.getvideo_fail));
                        return;
                    }

                    videoPath = w_strPath;
                    getThumbnail(w_strPath);

                }
                break;

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        /*imv_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                        if (_photoPath.length() > 0) {
                            txv_addphoto.setText("");
                            imv_play.setVisibility(View.GONE);
                        }*/

                        imv_photo.setImageResource(R.drawable.ic_img1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void getThumbnail(String _videoPath){

        File saveFile = BitmapUtils.getOutputMediaFile(this);

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(_videoPath, MediaStore.Images.Thumbnails.MINI_KIND);

        _thumPath = saveFile.getAbsolutePath();

        _thumbPath = getThumPath(thumb);

        addVideo(_videoPath, _thumbPath);

    }

    private void addVideo(String path, String thumb){

        Bitmap bitmap = BitmapFactory.decodeFile(thumb);
        imv_photo.setImageBitmap(bitmap);

        if (path.length() > 0) imv_play.setVisibility(View.VISIBLE);


    }

    private String getThumPath(Bitmap thumb) {

        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Temp";
        File dir = new File(file_path);
        if(!dir.exists())
            dir.mkdirs();

        long random = new Date().getTime();

        File file = new File(dir, "temp_ex" + random + ".png");

        try {

            FileOutputStream fOut = new FileOutputStream(file);

            thumb.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getAbsolutePath();
    }

    public void showChoiceDialog(final  int idx){

        String [][] itemsArray = {Constants.SUBPOST};
        final String items[] = itemsArray[idx];

        final TextView[] txvs = {ui_txvSubPost};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(strIds[idx]);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int item) {
                txvs[idx].setText(items[item]);
                _selectIdx[idx] = item;

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onClick(View view) {


        switch (view.getId()){

            case R.id.lyt_noty:
                gotoNoti();
                break;

            case R.id.lyt_post:
                gotoPost();
                break;

            case R.id.lyt_inspire:
                gotoInspire();
                break;

            case R.id.lyt_explore:
                gotoExplore();
                break;
            case R.id.lyt_profile:
                gotoProfile();
                break;

            case R.id.btn_sub_post:
                showChoiceDialog(0);
                break;

            case R.id.imv_addphoto:
                selectPhoto();
                break;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {

                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void checkLocationPermission() {

        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CAPTURE_VIDEO_OUTPUT};

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }

    }

    /** A method to download json data from url */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;

        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();


            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key=AIzaSyB_R9NUFK4G5lf-Zpr1wUmXDQ0WWZecYzA";

            String input="";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input+"&"+types+"&"+sensor+"&"+key;

            Log.d("Parameters :", "------------------>"+parameters);

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;

            Log.d("URL :", "====================>"+url);

            try{
                // Fetching the data from we service
                data = downloadUrl(url);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            edt_location.setAdapter(adapter);
        }
    }

    @OnClick(R.id.btn_post)void postActivity(){

        showToast("Clicked post");
    }


    @Override
    public void onBackPressed() {
        onExit();
    }
}
