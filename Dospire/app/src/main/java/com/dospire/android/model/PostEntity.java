package com.dospire.android.model;

import java.io.Serializable;

/**
 * Created by YueXi on 7/7/2017.
 */

public class PostEntity implements Serializable {

    int id = 0;
    String title = "";
    String location = "";
    String photo_url = "";
    int _photo_url  = 0;
    double rating;
    int vote_count = 0;
    int like_count = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public int getLike_count() {
        return like_count;
    }

    public void setLike_count(int like_count) {
        this.like_count = like_count;
    }

    public int get_photo_url() {
        return _photo_url;
    }

    public void set_photo_url(int _photo_url) {
        this._photo_url = _photo_url;
    }
}
