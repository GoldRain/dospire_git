package com.dospire.android.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dospire.android.R;
import com.dospire.android.activity.InspireActivity;
import com.dospire.android.model.UserEntity;
import com.dospire.android.utils.RadiusImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ITWhiz4U on 7/8/2017.
 */
public class InspireListAdapter extends BaseAdapter {

    InspireActivity _activity;
    ArrayList<UserEntity> _allInspire = new ArrayList<>();


    int vote_num = 0;

    public InspireListAdapter (InspireActivity activity, ArrayList<UserEntity> inspire){

        this._activity  = activity;
        this._allInspire = inspire;
    }

    @Override
    public int getCount() {
        return _allInspire.size();
    }

    @Override
    public Object getItem(int position) {
        return _allInspire.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final InspireHolder holder;

        if (convertView == null){

            holder = new InspireHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView  = inflater.inflate(R.layout.item_inspire_list, parent, false);

            holder.imv_photo = (CircleImageView) convertView.findViewById(R.id.imv_photo);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_location = (TextView)convertView.findViewById(R.id.txv_location);
            holder.imv_background = (ImageView)convertView.findViewById(R.id.imv_background);
            holder.imv_like = (ImageView)convertView.findViewById(R.id.imv_like);
            holder.imv_chatting = (ImageView)convertView.findViewById(R.id.imv_chatting);
            holder.imv_vote_plu = (ImageView)convertView.findViewById(R.id.imv_vote_plu);
            holder.imv_vote_min = (ImageView)convertView.findViewById(R.id.imv_vote_min);
            holder.txv_vote_num = (TextView) convertView.findViewById(R.id.txv_vote_num);
            holder.txv_post_num = (TextView) convertView.findViewById(R.id.txv_post_num);

            convertView.setTag(holder);
        } else {

            holder = (InspireHolder)convertView.getTag();
        }


        final UserEntity inspire = (UserEntity)_allInspire.get(position);

        Glide.with(_activity).load(inspire.get_photo_url()).placeholder(R.drawable.bg_non_profile).into(holder.imv_photo);

        holder.txv_name.setText(inspire.getUsername());
        holder.txv_location.setText(inspire.getLocation());
        Glide.with(_activity).load(inspire.get_bg_url()).placeholder(R.drawable.ic_img1).into(holder.imv_background);
        holder.txv_post_num.setText(String.valueOf(position));
        holder.txv_vote_num.setText(String.valueOf(position));

        holder.imv_like.setSelected(false);

        holder.imv_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.imv_like.setSelected(!holder.imv_like.isSelected());
            }
        });

        holder.imv_chatting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // _activity.gotoChatting(inspire);

            }
        });

        holder.imv_vote_plu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vote_num = Integer.parseInt(holder.txv_vote_num.getText().toString());

                holder.txv_vote_num.setText(String.valueOf(vote_num + 1));
            }
        });

        holder.imv_vote_min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                vote_num = Integer.parseInt(holder.txv_vote_num.getText().toString());

                if (vote_num == 0) {
                    Toast.makeText(_activity, "0...", Toast.LENGTH_SHORT).show();
                }
                else holder.txv_vote_num.setText(String.valueOf(vote_num - 1));
            }
        });

        return convertView;
    }



    public class InspireHolder {

        CircleImageView imv_photo;
        TextView txv_name, txv_location, txv_post_num, txv_vote_num;
        ImageView imv_background, imv_like, imv_chatting, imv_vote_plu, imv_vote_min;
    }
}
