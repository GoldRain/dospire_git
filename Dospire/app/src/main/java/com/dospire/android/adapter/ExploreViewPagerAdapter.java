package com.dospire.android.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dospire.android.R;
import com.dospire.android.activity.ExploreActivity;
import com.dospire.android.fragment.AllPostFragment;
import com.dospire.android.fragment.AvailableFragment;
import com.dospire.android.fragment.OtherFragment;
import com.dospire.android.fragment.RandomFragment;
import com.dospire.android.fragment.TopListFragment;

/**
 * Created by ITWhiz4U on 7/9/2017.
 */
public class ExploreViewPagerAdapter extends FragmentStatePagerAdapter {

    ExploreActivity _activity;

    public ExploreViewPagerAdapter(FragmentManager fm, ExploreActivity activity) {
        super(fm);
        this._activity = activity;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position){

            case 0:
                fragment = new AllPostFragment(_activity);
                break;

            case 1:
                fragment = new TopListFragment(_activity);
                break;

            case 2:
                fragment = new RandomFragment(_activity);
                break;

            case 3:
                fragment = new OtherFragment(_activity);
                break;

            case 4:
                fragment = new AvailableFragment(_activity);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String title = "";

        switch (position){

            case 0:
                title = _activity.getString(R.string.all_post);
                break;

            case 1:
                title = _activity.getString(R.string.top_list);
                break;

            case 2:
                title = _activity.getString(R.string.available);
                break;

            case 3:
                title = _activity.getString(R.string.random);
                break;

            case 4:
                title = _activity.getString(R.string.others);
                break;
        }

        return title;
    }
}
