package com.dospire.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.dospire.android.R;
import com.dospire.android.adapter.ExploreViewPagerAdapter;
import com.dospire.android.base.CommonActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExploreActivity extends CommonActivity {

    ViewPager viewPager;
    ExploreViewPagerAdapter _adapter;
    //PagerSlidingTabStrip tabLayout;

    @BindView(R.id.imv_inspire) ImageView imv_inspire;
    @BindView(R.id.imv_explore) ImageView imv_explore;
    @BindView(R.id.imv_post) ImageView imv_post;
    @BindView(R.id.imv_noty) ImageView imv_noty;
    @BindView(R.id.imv_profile) ImageView imv_profile;

    @BindView(R.id.drawerlayout) DrawerLayout ui_drawerlayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);
        ButterKnife.bind(this);

        loadLayout();
    }

    private void loadLayout() {

        FragmentManager manager = getSupportFragmentManager();
        _adapter = new ExploreViewPagerAdapter(manager, this);
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        viewPager.setAdapter(_adapter);

        //tabLayout = (PagerSlidingTabStrip)findViewById(R.id.tabs);
        //tabLayout.setupWithViewPager(viewPager);
        //tabLayout.setViewPager(viewPager);

        setFood(R.drawable.ic_inspire_g,R.drawable.ic_explore_b, R.drawable.ic_post_g, R.drawable.ic_noty_g, R.drawable.ic_profile_g);

    }

    @OnClick(R.id.imv_plus)void showPlus(){

        showToast("Click..");
    }

    @OnClick({R.id.imv_e,R.id.imv_t, R.id.imv_f,R.id.imv_tt, R.id.imv_d}) void showE(){

        showToast("Click..");
        ui_drawerlayout.closeDrawers();
    }


    public void setFood(int a, int b, int c, int d, int e){

        imv_inspire.setImageResource(a); imv_explore.setImageResource(b);imv_post.setImageResource(c); imv_noty.setImageResource(d);  imv_profile.setImageResource(e);

    }

    @OnClick(R.id.lyt_noty) void gotoNoti(){

        startActivity(new Intent(this, NotificationActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_post) void gotoPost(){
        startActivity(new Intent(this, PostActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_inspire) void gotoInspire(){

        startActivity(new Intent(this, InspireActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @OnClick(R.id.lyt_explore) void gotoExplore(){

        startActivity(new Intent(this, ExploreActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @OnClick(R.id.lyt_profile) void gotoProfile(){

        startActivity(new Intent(this, AccountActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @Override
    public void onBackPressed() {

        if (ui_drawerlayout.isDrawerOpen(GravityCompat.START)){

            ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else onExit();
    }
}
