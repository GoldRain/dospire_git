package com.dospire.android.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dospire.android.R;
import com.dospire.android.activity.ExploreActivity;
import com.dospire.android.activity.InspireActivity;
import com.dospire.android.model.PostEntity;
import com.dospire.android.model.UserEntity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ITWhiz4U on 7/9/2017.
 */
public class ExploreListViewAdapter extends BaseAdapter{

    private static final int TYPE_RIGHT = 0;
    private static final int TYPE_LEFT = 1;


    ExploreActivity _activity;
    ArrayList<UserEntity> _allInspire = new ArrayList<>();

    int vote_num = 0;

    public ExploreListViewAdapter (ExploreActivity activity, ArrayList<UserEntity> inspire){

        this._activity  = activity;
        this._allInspire = inspire;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {

        if (position%2  ==  1){

            return TYPE_LEFT;
        }

        return TYPE_RIGHT;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type){

            case TYPE_LEFT: {

               /* ExploreHolder_Left holder;

                if (convertView == null){

                    holder = new ExploreHolder_Left();*/

                    LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_explore_left, parent, false);

                   /* holder.imv_first_l = (ImageView)convertView.findViewById(R.id.imv_first);
                    holder.imv_second_l = (ImageView)convertView.findViewById(R.id.imv_second);
                    holder.imv_third_l = (ImageView)convertView.findViewById(R.id.imv_third);

                    convertView.setTag(holder);

                } else {

                    holder = (ExploreHolder_Left) convertView.getTag();
                }

                UserEntity postEntity = _allInspire.get(position);

                Glide.with(_activity).load(postEntity.get_photo_url()).placeholder(R.drawable.ic_img1).into(holder.imv_first_l);
                Glide.with(_activity).load(postEntity.get_photo_url()).placeholder(R.drawable.ic_img1).into(holder.imv_second_l);
                Glide.with(_activity).load(postEntity.get_photo_url()).placeholder(R.drawable.ic_img1).into(holder.imv_third_l);*/
            }

            break;

            case TYPE_RIGHT: {


               /* ExploreHolder_Right holder;

                if (convertView == null){

                    holder = new ExploreHolder_Right();*/
                    LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_explore_right, parent, false);
            }
            break;

                   /* holder.imv_first = (ImageView)convertView.findViewById(R.id.imv_first);
                    holder.imv_second = (ImageView)convertView.findViewById(R.id.imv_second);
                    holder.imv_third = (ImageView)convertView.findViewById(R.id.imv_third);

                    convertView.setTag(holder);

                }  else {

                    holder = (ExploreHolder_Right) convertView.getTag();
                }

                UserEntity postEntity = _allInspire.get(position);

                Glide.with(_activity).load(postEntity.get_bg_url()).placeholder(R.drawable.ic_img1).into(holder.imv_first);
                Glide.with(_activity).load(postEntity.get_bg_url()).placeholder(R.drawable.ic_img1).into(holder.imv_second);
                Glide.with(_activity).load(postEntity.get_bg_url()).placeholder(R.drawable.ic_img1).into(holder.imv_third);
            }*/
        }

        return convertView;
    }



    public class ExploreHolder_Left {

        ImageView imv_first_l, imv_second_l, imv_third_l;
    }


    public class ExploreHolder_Right {

        ImageView imv_first, imv_second, imv_third;
    }
}
