package com.dospire.android.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.dospire.android.R;
import com.dospire.android.activity.ExploreActivity;
import com.dospire.android.adapter.ExploreListViewAdapter;
import com.dospire.android.commons.Constants;


@SuppressLint("ValidFragment")
public class AvailableFragment extends Fragment {

    ExploreActivity _activity;
    View view;
    ListView lst_available;
    ExploreListViewAdapter _adapter_available;

    public AvailableFragment(ExploreActivity activity) {
        // Required empty public constructor
        this._activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_available, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        lst_available = (ListView)view.findViewById(R.id.lst_available);
        _adapter_available = new ExploreListViewAdapter(_activity, Constants.Common_User);
        lst_available.setAdapter(_adapter_available);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (ExploreActivity) context;
    }

}
