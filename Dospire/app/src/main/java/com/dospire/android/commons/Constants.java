package com.dospire.android.commons;

import com.dospire.android.model.UserEntity;

import java.util.ArrayList;

/**
 * Created by Snowflake on 7/27/2016.
 */
public class Constants {

    public static final int SPLASH_TIME = 2000;
    public static final int PROFILE_IMAGE_SIZE = 256;
    public static final int VOLLEY_TIME_OUT = 60000;

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int PICK_FROM_VIDEO = 104;
    public static final int REQUEST_CODE = 200;

    public static final String KEY_LOGOUT = "logout";
    public static final String KEY_CHATTING = "chatting";

    public static ArrayList<UserEntity> Common_User = new ArrayList<>();

    public static int CHAT_LIST = 0;

    public static final String [] SUBPOST = {"3 sec","4 sec","5 sec","6 sec","7 sec","8 sec","9 sec","10 sec","Forever"};


}
