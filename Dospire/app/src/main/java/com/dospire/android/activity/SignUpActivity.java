package com.dospire.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.dospire.android.R;
import com.dospire.android.base.CommonActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends CommonActivity {

    @BindView(R.id.edt_uname)EditText ui_edtUname;
    @BindView(R.id.edt_email)EditText ui_edtEmail;
    @BindView(R.id.edt_pwd)EditText ui_edtPwd;
    @BindView(R.id.edt_confirm)EditText ui_edtConfirm;
    @BindView(R.id.activity_signup)LinearLayout lltContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        loadLayout();
    }

    public void loadLayout(){

        // container

        lltContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtUname.getWindowToken(), 0);
                return false;
            }
        });

    }

    @OnClick({R.id.txv_signup, R.id.txv_signin}) void gotoSignIn(){

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.EXTRA_DOCK_STATE_CAR);
        startActivity(intent);
    }
}
